exports.helloWorld = function() {
    console.log("Hello world! (Scala.js calling Javascript)");
}

exports.foo = 1;

exports.bar = function (i) {
    return i + 1
};