console.log("Hello world! (Javascript)");

var {common} = require('scalajs/client-fastopt.js');

console.log("Hello world! (Javascript calling Scala.js)");
console.log("  baz: " + common.baz);

var React = require('node_modules/react');
var ReactDOM = require('node_modules/react-dom');

class HelloMessage extends React.Component {
    render() {
      return (
        <div>
          Hello {this.props.name}! (React)
        </div>
      );
    }
  }
  
ReactDOM.render(
    <HelloMessage name="World" />,
    document.getElementById('app')
);