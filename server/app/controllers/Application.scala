package controllers

import javax.inject._
import play.api.mvc._
import play.api.routing.JavaScriptReverseRouter

class Application extends InjectedController {

  def index = Action {
    Ok(views.html.index())
  }

}
