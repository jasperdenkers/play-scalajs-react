# Play + Scala.js + React

An example server/client project with Play in the backend, [Scala.js](https://www.scala-js.org/) and [React](https://reactjs.org/) in the frontend, and shared frontend/backend Scala code.

This example builds on the [Scala.js + React](https://gitlab.com/jasperdenkers/scalajs-react) example.

## Prerequisites 
 
 - The [SBT](https://www.scala-sbt.org/download.html) build tool

## Getting Started

Run SBT with `sbt`.
The project is divided in three modules `client`, `server`, and `shared`.
On SBT startup, the webpack development server is spawned automatically in the client project.
Additionally, SBT automatically enters the `server` project.
Manually switch between projects using e.g. `project client` in the SBT shell.

Run the application by running `run` in the SBT shell.

The application will be available at [localhost:9000](http://localhost:9000).

`Hello world!` is reported from several sources, demonstrating the integration of Javascript generated from Scala.js, regular Javascipt, Scala.js and Javascript calling each other, and React.

## Development

Changes to Javascript sources will be automatically live-reloaded.
A refresh is needed to trigger recompilation of Scala/Scala.js sources.
